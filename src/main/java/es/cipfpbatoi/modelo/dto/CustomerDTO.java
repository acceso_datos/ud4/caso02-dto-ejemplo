package es.cipfpbatoi.modelo.dto;
// Generated 31 ene 2023 11:09:24 by Hibernate Tools 4.3.6.Final

import lombok.Data;

/* By adding the @Getter and @Setter annotations, 
 * we told Lombok to generate these for all the fields of the class. 
 * @NoArgsConstructor will lead to an empty constructor generation.
 * @RequiredArgsConstructor annotation, we'll get a constructor for all the fields
 * @Data is a convenient shortcut annotation that bundles the features of @ToString, 
 * @EqualsAndHashCode, @Getter / @Setter and @RequiredArgsConstructor together
 * https://www.baeldung.com/intro-to-project-lombok
 * https://projectlombok.org/
*/

@Data
public class CustomerDTO implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private Short customerId;
	private String fullName; // firstName + lastName
	private String email;

	private String address;
	private String district;
	private String city;
	private String country;

}
