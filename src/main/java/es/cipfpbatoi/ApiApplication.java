package es.cipfpbatoi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*
 * Este proyecto utiliza la librería Lombok
 * Para que funcione se debe instalar la librería
 * para el IDE que estés utilizando.
 * Instrucciones en: https://projectlombok.org/
 * Una vez instalado, recuerda actualizar el proyecto.
 */


@SpringBootApplication
public class ApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiApplication.class, args);
	}

}
