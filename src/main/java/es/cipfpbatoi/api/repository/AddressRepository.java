package es.cipfpbatoi.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.repository.CrudRepository;

import es.cipfpbatoi.modelo.Address;

public interface AddressRepository extends JpaRepository<Address, Short> {

}
