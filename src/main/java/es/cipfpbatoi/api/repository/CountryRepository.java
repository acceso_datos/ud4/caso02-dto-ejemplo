package es.cipfpbatoi.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.repository.CrudRepository;

import es.cipfpbatoi.modelo.Country;

public interface CountryRepository extends JpaRepository<Country, Short> {

}

//public interface ClienteRepository extends CrudRepository<Cliente, Integer>{
//
//}
