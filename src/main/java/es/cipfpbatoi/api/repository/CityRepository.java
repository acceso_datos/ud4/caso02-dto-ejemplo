package es.cipfpbatoi.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.repository.CrudRepository;

import es.cipfpbatoi.modelo.City;

public interface CityRepository extends JpaRepository<City, Short> {

}

//public interface ClienteRepository extends CrudRepository<Cliente, Integer>{
//
//}
