package es.cipfpbatoi.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.repository.CrudRepository;

import es.cipfpbatoi.modelo.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Short> {

}

//public interface ClienteRepository extends CrudRepository<Cliente, Integer>{
//
//}
