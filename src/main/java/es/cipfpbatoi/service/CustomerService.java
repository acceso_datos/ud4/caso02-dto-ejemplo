package es.cipfpbatoi.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.cipfpbatoi.api.repository.CustomerRepository;
import es.cipfpbatoi.modelo.Address;
import es.cipfpbatoi.modelo.City;
import es.cipfpbatoi.modelo.Country;
import es.cipfpbatoi.modelo.Customer;
import es.cipfpbatoi.modelo.dto.CustomerDTO;
import jakarta.transaction.Transactional;

@Service
@Transactional
public class CustomerService {
	@Autowired
	private CustomerRepository customerRespository;
	
	public CustomerService() {		
	}

	public List<CustomerDTO> listAllCustomers() {
		List<Customer> customers = customerRespository.findAll();
		List<CustomerDTO> customersDto = new ArrayList<CustomerDTO>();
		
		for(Customer c: customers) {
			customersDto.add(mapCustomerToDto(c));
		}
		return customersDto;
	}

	public CustomerDTO getCustomer(Short id) {
		Customer customer = customerRespository.findById(id).get();
		CustomerDTO customerDto = mapCustomerToDto(customer);
		
		return customerDto;
	}
	
	private CustomerDTO mapCustomerToDto(Customer customer) {
		Address address = customer.getAddress();
		City city = address.getCity();
		Country country = city.getCountry();
		
		// mapeando de modelo a dto
		CustomerDTO customerDto = new CustomerDTO();
		customerDto.setCustomerId(customer.getCustomerId());
		customerDto.setFullName(customer.getFirstName() + " " + customer.getLastName());
		customerDto.setEmail(customer.getEmail());
		customerDto.setAddress(address.getAddress());
		customerDto.setDistrict(address.getDistrict());
		customerDto.setCity(city.getCity());
		customerDto.setCountry(country.getCountry());
		
		return customerDto;
	}
	
//	private Customer mapDtoToCustomer(CustomerDTO customerDto) {
//		Customer customer = new Customer();
//		return customer;
//	}

}
