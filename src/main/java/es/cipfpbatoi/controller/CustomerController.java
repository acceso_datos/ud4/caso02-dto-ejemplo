package es.cipfpbatoi.controller;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.cipfpbatoi.modelo.dto.CustomerDTO;
import es.cipfpbatoi.service.CustomerService;

@RestController
@RequestMapping("/customers")
public class CustomerController {
	@Autowired
	CustomerService customerService;

	@GetMapping("")
	public List<CustomerDTO> list() {
		return customerService.listAllCustomers();
	}

	@GetMapping("/{id}")
	public ResponseEntity<CustomerDTO> get(@PathVariable Short id) {
		try {
			CustomerDTO customerDto = customerService.getCustomer(id);
			return new ResponseEntity<CustomerDTO>(customerDto, HttpStatus.OK);
		} catch (NoSuchElementException e) {
			return new ResponseEntity<CustomerDTO>(HttpStatus.NOT_FOUND);
		}
	}




}
